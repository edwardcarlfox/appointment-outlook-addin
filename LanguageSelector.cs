﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppointmentOutlookAddIn
{
    public partial class LanguageSelector : Form
    {
        public LanguageSelector()
        {
            InitializeComponent();

            this.langCheckedListBox.CheckOnClick = true;

            foreach (string language in Properties.Settings.Default.Languages)
                if (language.StartsWith("//"))
                    this.langCheckedListBox.Items.Add(language.Substring(2));
                else
                    this.langCheckedListBox.Items.Add(language, true);
            this.langCheckedListBox.ItemCheck += langCheckedListBox_ItemCheck;
        }

        void langCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < Properties.Settings.Default.Languages.Count; i++)
            {
                string language = this.langCheckedListBox.Items[e.Index] as string;
                if (Properties.Settings.Default.Languages[i].EndsWith(language))
                {
                    Properties.Settings.Default.Languages.RemoveAt(i);
                    Properties.Settings.Default.Languages.Insert(i, e.NewValue == CheckState.Checked ? language : "//" + language);
                }
            }
            Properties.Settings.Default.Save();
        }
    }
}
