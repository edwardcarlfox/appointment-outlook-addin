﻿namespace AppointmentOutlookAddIn
{
    partial class AddInRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public AddInRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddInRibbon));
            this.addInTab = this.Factory.CreateRibbonTab();
            this.appTaskGroup = this.Factory.CreateRibbonGroup();
            this.appointmentButton = this.Factory.CreateRibbonButton();
            this.langSettingButton = this.Factory.CreateRibbonButton();
            this.addInTab.SuspendLayout();
            this.appTaskGroup.SuspendLayout();
            // 
            // addInTab
            // 
            this.addInTab.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.addInTab.Groups.Add(this.appTaskGroup);
            this.addInTab.Label = "Add-ins";
            this.addInTab.Name = "addInTab";
            // 
            // appTaskGroup
            // 
            this.appTaskGroup.Items.Add(this.appointmentButton);
            this.appTaskGroup.Items.Add(this.langSettingButton);
            this.appTaskGroup.Label = "Appointments & Tasks";
            this.appTaskGroup.Name = "appTaskGroup";
            // 
            // appointmentButton
            // 
            this.appointmentButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.appointmentButton.Image = ((System.Drawing.Image)(resources.GetObject("appointmentButton.Image")));
            this.appointmentButton.Label = "Create Appointment";
            this.appointmentButton.Name = "appointmentButton";
            this.appointmentButton.ScreenTip = "Create appointment recognized from the E-mail if any.";
            this.appointmentButton.ShowImage = true;
            // 
            // langSettingButton
            // 
            this.langSettingButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.langSettingButton.Image = ((System.Drawing.Image)(resources.GetObject("langSettingButton.Image")));
            this.langSettingButton.Label = "Language Setting";
            this.langSettingButton.Name = "langSettingButton";
            this.langSettingButton.ScreenTip = "Select languages to be recognized.";
            this.langSettingButton.ShowImage = true;
            // 
            // AddInRibbon
            // 
            this.Name = "AddInRibbon";
            this.RibbonType = "Microsoft.Outlook.Explorer, Microsoft.Outlook.Mail.Compose, Microsoft.Outlook.Mai" +
    "l.Read";
            this.Tabs.Add(this.addInTab);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.AddInRibbon_Load);
            this.addInTab.ResumeLayout(false);
            this.addInTab.PerformLayout();
            this.appTaskGroup.ResumeLayout(false);
            this.appTaskGroup.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab addInTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup appTaskGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton appointmentButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton langSettingButton;
    }

    partial class ThisRibbonCollection
    {
        internal AddInRibbon AddInRibbon
        {
            get { return this.GetRibbon<AddInRibbon>(); }
        }
    }
}
