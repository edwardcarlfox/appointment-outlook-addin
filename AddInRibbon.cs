﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Office.Tools.Ribbon;
using olNS = Microsoft.Office.Interop.Outlook;
using wfNS = System.Windows.Forms;
using System.Globalization;

namespace AppointmentOutlookAddIn
{
    public partial class AddInRibbon
    {
        DateTimeRecognizer dtRecognizer;

        private void AddInRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            this.appointmentButton.Click += appointmentButton_Click;
            this.langSettingButton.Click += langSettingButton_Click;
            this.loadLanguages();
        }

        private void loadLanguages()
        {
            this.dtRecognizer = new DateTimeRecognizer();

            foreach (string language in Properties.Settings.Default.Languages)
                if (!language.StartsWith("//"))
                    try
                    {
                        this.dtRecognizer.AddCulture(CultureInfo.GetCultureInfo(language.Substring(0, language.IndexOf("("))));
                    }
                    catch (ArgumentOutOfRangeException)
                    { }
                    catch (CultureNotFoundException)
                    { }
            this.dtRecognizer.AddCulture(CultureInfo.CurrentCulture);
        }

        private void langSettingButton_Click(object sender, RibbonControlEventArgs e)
        {
            (new LanguageSelector()).ShowDialog();
            this.loadLanguages();
        }

        private void appointmentButton_Click(object sender, RibbonControlEventArgs e)
        {
            olNS.Application olApp = Globals.ThisAddIn.Application;
            object window = olApp.ActiveWindow();
            //check the window type to determine where the button is clicked
            if (window is olNS.Explorer) //clicked on the explorer
            {
                olNS.Selection selection = (window as olNS.Explorer).Selection;
                if (selection.Count != 1)
                    wfNS.MessageBox.Show("Please selection a single item.", 
                        "Warning", wfNS.MessageBoxButtons.OK, wfNS.MessageBoxIcon.Warning);
                
                //get the first and the only item
                System.Collections.IEnumerator itemEnumerator = selection.GetEnumerator();
                itemEnumerator.MoveNext();
                this.createAppointment(itemEnumerator.Current);
            }
            else if (window is olNS.Inspector) //clicked on the inspector (either reading or composing a mail)
            {
                this.createAppointment((window as olNS.Inspector).CurrentItem);
            }
        }

        private void createAppointment(object item)
        {
            if (!(item is olNS.MailItem))
            {
                wfNS.MessageBox.Show("Please selection an E-Mail.",
                    "Warning", wfNS.MessageBoxButtons.OK, wfNS.MessageBoxIcon.Warning);
                return;
            }
            olNS.MailItem mail = item as olNS.MailItem;
            Dictionary<DateTime, string> dateTimeTextMap = new Dictionary<DateTime,string>();
            this.dtRecognizer.Parse(mail.Body, dateTimeTextMap);
            if (dateTimeTextMap.Count == 0)
            {
                wfNS.MessageBox.Show("No appointment is found in this E-mail.\n" + 
                    "Try to select the language being used in this E-mail in Language Setting.", 
                    "Info", wfNS.MessageBoxButtons.OK, wfNS.MessageBoxIcon.Information);
                return;
            }
            else if (dateTimeTextMap.Count == 1)
                this.showAppointmentWindows(mail, dateTimeTextMap.ToArray<KeyValuePair<DateTime, string>>()[0].Key);
            else
            {
                DateTimeChooser dtChooser = new DateTimeChooser();
                foreach (KeyValuePair<DateTime, string> dateTimeText in dateTimeTextMap)
                    dtChooser.AddDateTime(dateTimeText.Key, dateTimeText.Value);
                if (dtChooser.ShowDialog() == wfNS.DialogResult.OK)
                    this.showAppointmentWindows(mail, dtChooser.SelectedDateTime);
            }
        }

        private void showAppointmentWindows(olNS.MailItem mail, DateTime dateTime)
        {
            olNS.Application olApp = Globals.ThisAddIn.Application;
            olNS.AppointmentItem appointment = olApp.CreateItem(olNS.OlItemType.olAppointmentItem);
            appointment.Subject = mail.Subject;
            appointment.RTFBody = mail.RTFBody;
            appointment.Start = dateTime;
            appointment.AllDayEvent = dateTime.TimeOfDay == TimeSpan.Zero;
            appointment.Display();
        }
    }
}
