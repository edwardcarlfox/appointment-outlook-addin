﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppointmentOutlookAddIn.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3." +
            "org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <s" +
            "tring>//af(Afrikaans)</string>\r\n  <string>//af-ZA(Afrikaans (Suid Afrika))</stri" +
            "ng>\r\n  <string>//sq(shqipe)</string>\r\n  <string>//sq-AL(shqipe (Shqipëria))</str" +
            "ing>\r\n  <string>//gsw-FR(Elsässisch (Frànkrisch))</string>\r\n  <string>//am-ET(አማ" +
            "ርኛ (ኢትዮጵያ))</string>\r\n  <string>//ar(العربية‏)</string>\r\n  <string>//ar-DZ(العرب" +
            "ية (الجزائر)‏)</string>\r\n  <string>//ar-BH(العربية (البحرين)‏)</string>\r\n  <stri" +
            "ng>//ar-EG(العربية (مصر)‏)</string>\r\n  <string>//ar-IQ(العربية (العراق)‏)</strin" +
            "g>\r\n  <string>//ar-JO(العربية (الأردن)‏)</string>\r\n  <string>//ar-KW(العربية (ال" +
            "كويت)‏)</string>\r\n  <string>//ar-LB(العربية (لبنان)‏)</string>\r\n  <string>//ar-L" +
            "Y(العربية (ليبيا)‏)</string>\r\n  <string>//ar-MA(العربية (المملكة المغربية)‏)</st" +
            "ring>\r\n  <string>//ar-OM(العربية (عمان)‏)</string>\r\n  <string>//ar-QA(العربية (ق" +
            "طر)‏)</string>\r\n  <string>//ar-SA(العربية (المملكة العربية السعودية)‏)</string>\r" +
            "\n  <string>//ar-SY(العربية (سوريا)‏)</string>\r\n  <string>//ar-TN(العربية (تونس)‏" +
            ")</string>\r\n  <string>//ar-AE(العربية (الإمارات العربية المتحدة)‏)</string>\r\n  <" +
            "string>//ar-YE(العربية (اليمن)‏)</string>\r\n  <string>//hy(Հայերեն)</string>\r\n  <" +
            "string>//hy-AM(Հայերեն (Հայաստան))</string>\r\n  <string>//as-IN(অসমীয়া (ভাৰত))</s" +
            "tring>\r\n  <string>//az(Azərbaycan­ılı)</string>\r\n  <string>//az-Cyrl-AZ(Азәрбајҹ" +
            "ан (Азәрбајҹан))</string>\r\n  <string>//az-Latn-AZ(Azərbaycan­ılı (Azərbaycanca))" +
            "</string>\r\n  <string>//ba-RU(Башҡорт (Россия))</string>\r\n  <string>//eu(euskara)" +
            "</string>\r\n  <string>//eu-ES(euskara (euskara))</string>\r\n  <string>//be(Беларус" +
            "кі)</string>\r\n  <string>//be-BY(Беларускі (Беларусь))</string>\r\n  <string>//bn-B" +
            "D(বাংলা (বাংলা))</string>\r\n  <string>//bn-IN(বাংলা (ভারত))</string>\r\n  <string>/" +
            "/bs-Cyrl-BA(босански (Босна и Херцеговина))</string>\r\n  <string>//bs-Latn-BA(bos" +
            "anski (Bosna i Hercegovina))</string>\r\n  <string>//br-FR(brezhoneg (Frañs))</str" +
            "ing>\r\n  <string>//bg(български)</string>\r\n  <string>//bg-BG(български (България)" +
            ")</string>\r\n  <string>//ca(català)</string>\r\n  <string>//ca-ES(català (català))<" +
            "/string>\r\n  <string>//zh-HK(中文(香港特别行政區))</string>\r\n  <string>//zh-MO(中文(澳門特别行政區)" +
            ")</string>\r\n  <string>//zh-CN(中文(中华人民共和国))</string>\r\n  <string>//zh-Hans(中文(简体))" +
            "</string>\r\n  <string>//zh-SG(中文(新加坡))</string>\r\n  <string>//zh-TW(中文(台灣))</strin" +
            "g>\r\n  <string>//zh-Hant(中文(繁體))</string>\r\n  <string>//co-FR(Corsu (France))</str" +
            "ing>\r\n  <string>//hr(hrvatski)</string>\r\n  <string>//hr-HR(hrvatski (Hrvatska))<" +
            "/string>\r\n  <string>//hr-BA(hrvatski (Bosna i Hercegovina))</string>\r\n  <string>" +
            "//cs(čeština)</string>\r\n  <string>//cs-CZ(čeština (Česká republika))</string>\r\n " +
            " <string>//da(dansk)</string>\r\n  <string>//da-DK(dansk (Danmark))</string>\r\n  <s" +
            "tring>//prs-AF(درى (افغانستان))</string>\r\n  <string>//div(ދިވެހިބަސް‏)</string>\r" +
            "\n  <string>//div-MV(ދިވެހިބަސް (ދިވެހި ރާއްޖެ)‏)</string>\r\n  <string>//nl(Nederl" +
            "ands)</string>\r\n  <string>//nl-BE(Nederlands (België))</string>\r\n  <string>//nl-" +
            "NL(Nederlands (Nederland))</string>\r\n  <string>//en(English)</string>\r\n  <string" +
            ">//en-AU(English (Australia))</string>\r\n  <string>//en-BZ(English (Belize))</str" +
            "ing>\r\n  <string>//en-CA(English (Canada))</string>\r\n  <string>//en-029(English (" +
            "Caribbean))</string>\r\n  <string>//en-IN(English (India))</string>\r\n  <string>//e" +
            "n-IE(English (Eire))</string>\r\n  <string>//en-JM(English (Jamaica))</string>\r\n  " +
            "<string>//en-MY(English (Malaysia))</string>\r\n  <string>//en-NZ(English (New Zea" +
            "land))</string>\r\n  <string>//en-PH(English (Philippines))</string>\r\n  <string>//" +
            "en-SG(English (Singapore))</string>\r\n  <string>//en-ZA(English (South Africa))</" +
            "string>\r\n  <string>//en-TT(English (Trinidad y Tobago))</string>\r\n  <string>//en" +
            "-GB(English (United Kingdom))</string>\r\n  <string>//en-US(English (United States" +
            "))</string>\r\n  <string>//en-ZW(English (Zimbabwe))</string>\r\n  <string>//et(eest" +
            "i)</string>\r\n  <string>//et-EE(eesti (Eesti))</string>\r\n  <string>//fo(føroyskt)" +
            "</string>\r\n  <string>//fo-FO(føroyskt (Føroyar))</string>\r\n  <string>//fil-PH(Fi" +
            "lipino (Pilipinas))</string>\r\n  <string>//fi(suomi)</string>\r\n  <string>//fi-FI(" +
            "suomi (Suomi))</string>\r\n  <string>//fr(français)</string>\r\n  <string>//fr-BE(fr" +
            "ançais (Belgique))</string>\r\n  <string>//fr-CA(français (Canada))</string>\r\n  <s" +
            "tring>//fr-FR(français (France))</string>\r\n  <string>//fr-LU(français (Luxembour" +
            "g))</string>\r\n  <string>//fr-MC(français (Principauté de Monaco))</string>\r\n  <s" +
            "tring>//fr-CH(français (Suisse))</string>\r\n  <string>//fy-NL(Frysk (Nederlân))</" +
            "string>\r\n  <string>//gl(galego)</string>\r\n  <string>//gl-ES(galego (galego))</st" +
            "ring>\r\n  <string>//ka(ქართული)</string>\r\n  <string>//ka-GE(ქართული (საქართველო))" +
            "</string>\r\n  <string>//de(Deutsch)</string>\r\n  <string>//de-AT(Deutsch (Österrei" +
            "ch))</string>\r\n  <string>//de-DE(Deutsch (Deutschland))</string>\r\n  <string>//de" +
            "-LI(Deutsch (Liechtenstein))</string>\r\n  <string>//de-LU(Deutsch (Luxemburg))</s" +
            "tring>\r\n  <string>//de-CH(Deutsch (Schweiz))</string>\r\n  <string>//el(ελληνικά)<" +
            "/string>\r\n  <string>//el-GR(ελληνικά (Ελλάδα))</string>\r\n  <string>//kl-GL(kalaa" +
            "llisut (Kalaallit Nunaat))</string>\r\n  <string>//gu(ગુજરાતી)</string>\r\n  <string" +
            ">//gu-IN(ગુજરાતી (ભારત))</string>\r\n  <string>//ha-Latn-NG(Hausa (Nigeria))</stri" +
            "ng>\r\n  <string>//he(עברית‏)</string>\r\n  <string>//he-IL(עברית (ישראל)‏)</string>" +
            "\r\n  <string>//hi(हिंदी)</string>\r\n  <string>//hi-IN(हिंदी (भारत))</string>\r\n  <s" +
            "tring>//hu(magyar)</string>\r\n  <string>//hu-HU(magyar (Magyarország))</string>\r\n" +
            "  <string>//is(íslenska)</string>\r\n  <string>//is-IS(íslenska (Ísland))</string>" +
            "\r\n  <string>//ig-NG(Igbo (Nigeria))</string>\r\n  <string>//id(Bahasa Indonesia)</" +
            "string>\r\n  <string>//id-ID(Bahasa Indonesia (Indonesia))</string>\r\n  <string>//i" +
            "u-Latn-CA(Inuktitut (Kanatami) (kanata))</string>\r\n  <string>//iu-Cans-CA(ᐃᓄᒃᑎᑐᑦ" +
            " (ᑲᓇᑕ))</string>\r\n  <string>//ga-IE(Gaeilge (Éire))</string>\r\n  <string>//xh-ZA(" +
            "isiXhosa (uMzantsi Afrika))</string>\r\n  <string>//zu-ZA(isiZulu (iNingizimu Afri" +
            "ka))</string>\r\n  <string>//it(italiano)</string>\r\n  <string>//it-IT(italiano (It" +
            "alia))</string>\r\n  <string>//it-CH(italiano (Svizzera))</string>\r\n  <string>//ja" +
            "(日本語)</string>\r\n  <string>//ja-JP(日本語 (日本))</string>\r\n  <string>//kn(ಕನ್ನಡ)</str" +
            "ing>\r\n  <string>//kn-IN(ಕನ್ನಡ (ಭಾರತ))</string>\r\n  <string>//kk(Қазащb)</string>\r" +
            "\n  <string>//kk-KZ(Қазақ (Қазақстан))</string>\r\n  <string>//km-KH(ខ្មែរ (កម្ពុជា" +
            "))</string>\r\n  <string>//qut-GT(K\'iche (Guatemala))</string>\r\n  <string>//rw-RW(" +
            "Kinyarwanda (Rwanda))</string>\r\n  <string>//sw(Kiswahili)</string>\r\n  <string>//" +
            "sw-KE(Kiswahili (Kenya))</string>\r\n  <string>//kok(कोंकणी)</string>\r\n  <string>/" +
            "/kok-IN(कोंकणी (भारत))</string>\r\n  <string>//ko(한국어)</string>\r\n  <string>//ko-KR" +
            "(한국어 (대한민국))</string>\r\n  <string>//ky(Кыргыз)</string>\r\n  <string>//ky-KG(Кыргыз" +
            " (Кыргызстан))</string>\r\n  <string>//lo-LA(ລາວ (ສ.ປ.ປ. ລາວ))</string>\r\n  <string" +
            ">//lv(latviešu)</string>\r\n  <string>//lv-LV(latviešu (Latvija))</string>\r\n  <str" +
            "ing>//lt(lietuvių)</string>\r\n  <string>//lt-LT(lietuvių (Lietuva))</string>\r\n  <" +
            "string>//wee-DE(dolnoserbšćina (Nimska))</string>\r\n  <string>//lb-LU(Lëtzebuerge" +
            "sch (Luxembourg))</string>\r\n  <string>//mk(македонски јазик)</string>\r\n  <string" +
            ">//mk-MK(македонски јазик (Македонија))</string>\r\n  <string>//ms(Bahasa Malaysia" +
            ")</string>\r\n  <string>//ms-BN(Bahasa Malaysia (Brunei Darussalam))</string>\r\n  <" +
            "string>//ms-MY(Bahasa Malaysia (Malaysia))</string>\r\n  <string>//ml-IN(മലയാളം (ഭ" +
            "ാരതം))</string>\r\n  <string>//mt-MT(Malti (Malta))</string>\r\n  <string>//mi-NZ(Re" +
            "o Māori (Aotearoa))</string>\r\n  <string>//arn-CL(Mapudungun (Chile))</string>\r\n " +
            " <string>//mr(मराठी)</string>\r\n  <string>//mr-IN(मराठी (भारत))</string>\r\n  <stri" +
            "ng>//moh-CA(Kanien\'kéha (Canada))</string>\r\n  <string>//mn(Монгол хэл)</string>\r" +
            "\n  <string>//mn-MN(Монгол хэл (Монгол улс))</string>\r\n  <string>//mn-Mong-CN(ᠮᠣᠩ" +
            "ᠭᠤᠯ ᠬᠡᠯᠡ (ᠪᠦᠭᠦᠳᠡ ᠨᠠᠢᠷᠠᠮᠳᠠᠬᠤ ᠳᠤᠮᠳᠠᠳᠤ ᠠᠷᠠᠳ ᠣᠯᠣᠰ))</string>\r\n  <string>//ne-NP(नेपा" +
            "ली (नेपाल))</string>\r\n  <string>//no(norsk)</string>\r\n  <string>//nb-NO(norsk, b" +
            "okmål (Norge))</string>\r\n  <string>//nn-NO(norsk, nynorsk (Noreg))</string>\r\n  <" +
            "string>//oc-FR(Occitan (França))</string>\r\n  <string>//or-IN(ଓଡ଼ିଆ (ଭାରତ))</strin" +
            "g>\r\n  <string>//ps-AF(پښتو (افغانستان))</string>\r\n  <string>//fa(فارسى‏)</string" +
            ">\r\n  <string>//fa-IR(فارسى (ايران)‏)</string>\r\n  <string>//pl(polski)</string>\r\n" +
            "  <string>//pl-PL(polski (Polska))</string>\r\n  <string>//pt(Português)</string>\r" +
            "\n  <string>//pt-BR(Português (Brasil))</string>\r\n  <string>//pt-PT(português (Po" +
            "rtugal))</string>\r\n  <string>//pa(ਪੰਜਾਬੀ)</string>\r\n  <string>//pa-IN(ਪੰਜਾਬੀ (ਭਾ" +
            "ਰਤ))</string>\r\n  <string>//quz-BO(runasimi (Bolivia Suyu))</string>\r\n  <string>/" +
            "/quz-EC(runasimi (Ecuador Suyu))</string>\r\n  <string>//quz-PE(runasimi (Peru Suy" +
            "u))</string>\r\n  <string>//ro(română)</string>\r\n  <string>//ro-RO(română (România" +
            "))</string>\r\n  <string>//rm-CH(Rumantsch (Svizra))</string>\r\n  <string>//ru(русс" +
            "кий)</string>\r\n  <string>//ru-RU(русский (Россия))</string>\r\n  <string>//smn-FI(" +
            "sämikielâ (Suomâ))</string>\r\n  <string>//smj-NO(julevusámegiella (Vuodna))</stri" +
            "ng>\r\n  <string>//smj-SE(julevusámegiella (Svierik))</string>\r\n  <string>//se-FI(" +
            "davvisámegiella (Suopma))</string>\r\n  <string>//se-NO(davvisámegiella (Norga))</" +
            "string>\r\n  <string>//se-SE(davvisámegiella (Ruoŧŧa))</string>\r\n  <string>//sms-F" +
            "I(sääm´ǩiõll (Lää´ddjânnam))</string>\r\n  <string>//sma-NO(åarjelsaemiengiele (Nö" +
            "örje))</string>\r\n  <string>//sma-SE(åarjelsaemiengiele (Sveerje))</string>\r\n  <s" +
            "tring>//sa(संस्कृत)</string>\r\n  <string>//sa-IN(संस्कृत (भारतम्))</string>\r\n  <s" +
            "tring>//sr(srpski)</string>\r\n  <string>//sr-Cyrl-BA(српски (Босна и Херцеговина)" +
            ")</string>\r\n  <string>//sr-Cyrl-SP(српски (Србија и Црна Гора))</string>\r\n  <str" +
            "ing>//sr-Latn-BA(srpski (Bosna i Hercegovina))</string>\r\n  <string>//sr-Latn-SP(" +
            "srpski (Srbija i Crna Gora))</string>\r\n  <string>//nso-ZA(Sesotho sa Leboa (Afri" +
            "ka Borwa))</string>\r\n  <string>//tn-ZA(Setswana (Aforika Borwa))</string>\r\n  <st" +
            "ring>//si-LK(සිංහ (ශ්‍රී ලංකා))</string>\r\n  <string>//sk(slovenčina)</string>\r\n " +
            " <string>//sk-SK(slovenčina (Slovenská republika))</string>\r\n  <string>//sl(slov" +
            "enski)</string>\r\n  <string>//sl-SI(slovenski (Slovenija))</string>\r\n  <string>//" +
            "es(español)</string>\r\n  <string>//es-AR(Español (Argentina))</string>\r\n  <string" +
            ">//es-BO(Español (Bolivia))</string>\r\n  <string>//es-CL(Español (Chile))</string" +
            ">\r\n  <string>//es-CO(Español (Colombia))</string>\r\n  <string>//es-CR(Español (Co" +
            "sta Rica))</string>\r\n  <string>//es-DO(Español (República Dominicana))</string>\r" +
            "\n  <string>//es-EC(Español (Ecuador))</string>\r\n  <string>//es-SV(Español (El Sa" +
            "lvador))</string>\r\n  <string>//es-GT(Español (Guatemala))</string>\r\n  <string>//" +
            "es-HN(Español (Honduras))</string>\r\n  <string>//es-MX(Español (México))</string>" +
            "\r\n  <string>//es-NI(Español (Nicaragua))</string>\r\n  <string>//es-PA(Español (Pa" +
            "namá))</string>\r\n  <string>//es-PY(Español (Paraguay))</string>\r\n  <string>//es-" +
            "PE(Español (Perú))</string>\r\n  <string>//es-PR(Español (Puerto Rico))</string>\r\n" +
            "  <string>//es-ES(español (España))</string>\r\n  <string>//es-US(Español (Estados" +
            " Unidos))</string>\r\n  <string>//es-UY(Español (Uruguay))</string>\r\n  <string>//e" +
            "s-VE(Español (Republica Bolivariana de Venezuela))</string>\r\n  <string>//sv(sven" +
            "ska)</string>\r\n  <string>//sv-FI(svenska (Finland))</string>\r\n  <string>//sv-SE(" +
            "svenska (Sverige))</string>\r\n  <string>//syr(ܣܘܪܝܝܐ‏)</string>\r\n  <string>//syr-" +
            "SY(ܣܘܪܝܝܐ (سوريا)‏)</string>\r\n  <string>//tg-Cyrl-TJ(Тоҷикӣ (Тоҷикистон))</strin" +
            "g>\r\n  <string>//tzm-Latn-DZ(Tamazight (Djazaïr))</string>\r\n  <string>//ta(தமிழ்)" +
            "</string>\r\n  <string>//ta-IN(தமிழ் (இந்தியா))</string>\r\n  <string>//tt(Татар)</s" +
            "tring>\r\n  <string>//tt-RU(Татар (Россия))</string>\r\n  <string>//te(తెలుగు)</stri" +
            "ng>\r\n  <string>//te-IN(తెలుగు (భారత దేశం))</string>\r\n  <string>//th(ไทย)</string" +
            ">\r\n  <string>//th-TH(ไทย (ไทย))</string>\r\n  <string>//bo-CN(བོད་ཡིག (ཀྲུང་ཧྭ་མི་" +
            "དམངས་སྤྱི་མཐུན་རྒྱལ་ཁབ།))</string>\r\n  <string>//tr(Türkçe)</string>\r\n  <string>/" +
            "/tr-TR(Türkçe (Türkiye))</string>\r\n  <string>//tk-TM(türkmençe (Türkmenistan))</" +
            "string>\r\n  <string>//ug-CN(ئۇيغۇر يېزىقى (جۇڭخۇا خەلق جۇمھۇرىيىتى))</string>\r\n  " +
            "<string>//uk(україньска)</string>\r\n  <string>//uk-UA(україньска (Україна))</stri" +
            "ng>\r\n  <string>//wen-DE(hornjoserbšćina (Němska))</string>\r\n  <string>//ur(اُردو" +
            "‏)</string>\r\n  <string>//ur-PK(اُردو (پاکستان)‏)</string>\r\n  <string>//uz(U\'zbek" +
            ")</string>\r\n  <string>//uz-Cyrl-UZ(Ўзбек (Ўзбекистон))</string>\r\n  <string>//uz-" +
            "Latn-UZ(U\'zbek (U\'zbekiston Respublikasi))</string>\r\n  <string>//vi(Tiếng Việt" +
            ")</string>\r\n  <string>//vi-VN(Tiếng Việt (Việt Nam))</string>\r\n  <string>//cy" +
            "-GB(Cymraeg (y Deyrnas Unedig))</string>\r\n  <string>//wo-SN(Wolof (Sénégal))</st" +
            "ring>\r\n  <string>//sah-RU(саха (Россия))</string>\r\n  <string>//ii-CN(ꆈꌠꁱꂷ (ꍏꉸꏓꂱꇭ" +
            "ꉼꇩ))</string>\r\n  <string>//yo-NG(Yoruba (Nigeria))</string>\r\n</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Languages {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Languages"]));
            }
            set {
                this["Languages"] = value;
            }
        }
    }
}
