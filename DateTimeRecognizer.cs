﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;

namespace AppointmentOutlookAddIn
{
    class DateTimeRecognizer
    {
        class TextInterval
        {
            public int Start { get; set; }
            public int End { get; set; }
            public int Length { get { return this.End - this.Start; } }

            public TextInterval(int start, int end)
            {
                this.Start = start;
                this.End = end;
            }

            public bool Includes(int start, int end)
            {
                if (this.Start <= start && this.End >= end)
                    return true;
                return false;
            }

            public bool Includes(TextInterval other)
            {
                return this.Includes(other.Start, other.End);
            }

            public override string ToString()
            {
                return this.Start.ToString() + "->" + this.End.ToString();
            }
        }

        private List<CultureInfo> cultures = new List<CultureInfo>();
        private List<Regex> dateTimeRegexs = new List<Regex>();

        /// <summary>
        /// Add a culture to the recognizer.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <remarks>Full list of all predefined culture names: http://msdn.microsoft.com/en-us/goglobal/bb896001.aspx </remarks>
        public void AddCulture(CultureInfo culture)
        {
            if (!this.cultures.Contains(culture))
            {
                cultures.Add(culture);
                foreach (string dateTimePattern in culture.DateTimeFormat.GetAllDateTimePatterns())
                    this.dateTimeRegexs.Add(new Regex(this.createRegexFromPattern(culture.DateTimeFormat, dateTimePattern)));
            }
        }

        public void Parse(string text, Dictionary<DateTime, string> dateTimeTextMap)
        {
            Dictionary<TextInterval, DateTime> intervalDateTimeMap = new Dictionary<TextInterval, DateTime>();
            Dictionary<TextInterval, string> intervalDateTimeTextMap = new Dictionary<TextInterval, string>();

            foreach (Regex dateTimeRegex in this.dateTimeRegexs)
            {
                MatchCollection matches = dateTimeRegex.Matches(text);
                foreach (Match match in matches)
                {
                    DateTime dateTime = new DateTime();
                    if (DateTime.TryParse(match.Value, out dateTime))
                    {
                        TextInterval interval = new TextInterval(match.Index, match.Index + match.Length);
                        intervalDateTimeMap.Add(interval, dateTime);
                        intervalDateTimeTextMap.Add(interval, match.Value);
                    }
                }
            }
            bool intervalFused = true;
            while (intervalFused)
            {
                intervalFused = false;
                TextInterval fusingInterval = null;
                foreach (KeyValuePair<TextInterval, DateTime> intervalDateTime1 in intervalDateTimeMap)
                {
                    foreach (KeyValuePair<TextInterval, DateTime> intervalDateTime2 in intervalDateTimeMap)
                    {
                        if (intervalDateTime1.Key != intervalDateTime2.Key)
                        {
                            if (intervalDateTime1.Key.Includes(intervalDateTime2.Key))
                            {
                                fusingInterval = intervalDateTime2.Key;
                                break;
                            }
                            else if (intervalDateTime2.Key.Includes(intervalDateTime1.Key))
                            {
                                fusingInterval = intervalDateTime1.Key;
                                break;
                            }
                        }
                    }
                    if (fusingInterval != null)
                        break;
                }
                if (fusingInterval != null)
                {
                    intervalDateTimeMap.Remove(fusingInterval);
                    intervalFused = true;
                }
            }
            foreach (KeyValuePair<TextInterval, DateTime> intervalDateTime in intervalDateTimeMap)
                if (!dateTimeTextMap.ContainsKey(intervalDateTime.Value))
                    dateTimeTextMap.Add(intervalDateTime.Value, intervalDateTimeTextMap[intervalDateTime.Key]);
        }

        private string createRegexFromPattern(DateTimeFormatInfo formatInfo, string pattern)
        {
            StringBuilder tempBuilder = new StringBuilder();
            StringBuilder regexBuilder = new StringBuilder();

            while (pattern.Length > 0)
            {
                if (pattern.IndexOf("dddd") == 0)
                {
                    tempBuilder.Append("(");
                    tempBuilder.Append(string.Join("|", formatInfo.DayNames));
                    tempBuilder.Append(")");
                    regexBuilder.Append(tempBuilder.ToString());
                    pattern = pattern.Remove(0, 4);
                }
                else if (pattern.IndexOf("ddd") == 0)
                {
                    tempBuilder.Clear();
                    tempBuilder.Append("(");
                    tempBuilder.Append(string.Join("|", formatInfo.AbbreviatedDayNames));
                    tempBuilder.Append(")");
                    regexBuilder.Append(tempBuilder.ToString());
                    pattern = pattern.Remove(0, 3);
                }
                else if (pattern.IndexOf("dd") == 0)
                {
                    regexBuilder.Append("(0[1-9]|(1|2)[0-9]|3(0|1))");
                    pattern = pattern.Remove(0, 2);
                }
                else if (pattern.IndexOf("d") == 0)
                {
                    regexBuilder.Append("([1-9]|(1|2)[0-9]|3(0|1))");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("hh") == 0)
                {
                    regexBuilder.Append("(0[1-9]|1[1-2])");
                    pattern = pattern.Remove(0, 2);
                }
                else if (pattern.IndexOf("h") == 0)
                {
                    regexBuilder.Append("([1-9]|1[1-2])");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("HH") == 0)
                {
                    regexBuilder.Append("(0[1-9]|1[0-9]|2[0-3])");
                    pattern = pattern.Remove(0, 2);
                }
                else if (pattern.IndexOf("H") == 0)
                {
                    regexBuilder.Append("([1-9]|1[0-9]|2[0-3])");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("mm") == 0)
                {
                    regexBuilder.Append("([0-5][0-9])");
                    pattern = pattern.Remove(0, 2);
                }
                else if (pattern.IndexOf("m") == 0)
                {
                    regexBuilder.Append("([0-9]|[1-5][0-9])");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("MMMM") == 0)
                {
                    tempBuilder.Clear();
                    tempBuilder.Append("(");
                    tempBuilder.Append(string.Join("|", formatInfo.MonthNames, 0, 12));
                    tempBuilder.Append(")");
                    regexBuilder.Append(tempBuilder.ToString());
                    pattern = pattern.Remove(0, 4);
                }
                else if (pattern.IndexOf("MMM") == 0)
                {
                    tempBuilder.Clear();
                    tempBuilder.Append("(");
                    tempBuilder.Append(string.Join("|", formatInfo.AbbreviatedMonthNames, 0, 12));
                    tempBuilder.Append(")");
                    regexBuilder.Append(tempBuilder.ToString());
                    pattern = pattern.Remove(0, 3);
                }
                else if (pattern.IndexOf("MM") == 0)
                {
                    regexBuilder.Append("(0[1-9]|1[1-2])");
                    pattern = pattern.Remove(0, 2);
                }
                else if (pattern.IndexOf("M") == 0)
                {
                    regexBuilder.Append("([1-9]|1[1-2])");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("yyyy") == 0)
                {
                    regexBuilder.Append("([0-9]{4})");
                    pattern = pattern.Remove(0, 4);
                }
                else if (pattern.IndexOf("yyy") == 0)
                {
                    regexBuilder.Append("([0-9]{3,4})");
                    pattern = pattern.Remove(0, 3);
                }
                else if (pattern.IndexOf("yy") == 0)
                {
                    regexBuilder.Append("([0-9]{2})");
                    pattern = pattern.Remove(0, 2);
                }
                else if (pattern.IndexOf("y") == 0)
                {
                    regexBuilder.Append("([0-9]|([0-9]{2}))");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("\\") == 0)
                {
                    regexBuilder.Append("\\\\");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf(" ") == 0)
                {
                    regexBuilder.Append("\\s+");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf(".") == 0)
                {
                    regexBuilder.Append("\\.");
                    pattern = pattern.Remove(0, 1);
                }
                else if (pattern.IndexOf("'") == 0 || //remove the string delimiter
                    //the following are ignored in normal hand written date time format
                    pattern.IndexOf("F") == 0 || pattern.IndexOf("f") == 0 ||
                    pattern.IndexOf("K") == 0 || pattern.IndexOf("g") == 0 ||
                    pattern.IndexOf("t") == 0 || pattern.IndexOf("z") == 0)
                    pattern = pattern.Remove(0, 1);
                else 
                {
                    regexBuilder.Append(pattern.Substring(0, 1));
                    pattern = pattern.Remove(0, 1);
                }
            }

            return regexBuilder.ToString();
        }
    }
}
