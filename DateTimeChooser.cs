﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppointmentOutlookAddIn
{
    public partial class DateTimeChooser : Form
    {
        private Dictionary<string, DateTime> dateTimeTextMap = new Dictionary<string, DateTime>();

        public DateTime SelectedDateTime { get; private set; }

        public DateTimeChooser()
        {
            InitializeComponent();

            this.dateListbox.MouseDoubleClick += dateListbox_MouseDoubleClick;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        void dateListbox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int itemIndex = this.dateListbox.IndexFromPoint(e.Location);
            if (itemIndex != ListBox.NoMatches)
            {
                this.SelectedDateTime = this.dateTimeTextMap[this.dateListbox.Items[itemIndex] as string];
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        public void AddDateTime(DateTime dateTime, string dateTimeText)
        { 
            this.dateListbox.Items.Add(dateTimeText);
            this.dateTimeTextMap.Add(dateTimeText, dateTime);
        }
    }
}
